package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.android.betapp.R;

/**
 * Created by hp on 04-07-2017.
 */

public class StartBetChatFragment extends BaseFragment {
    Toolbar toolbar;
    ImageButton img_ToolBarAdd;
    TabLayout tabLayout;
    ViewPager viewpager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_bet_chat,container,false);
        initView(view);
        initTab();
        return view;

    }

    private void initView(View view) {
        toolbar = (Toolbar)view.findViewById(R.id.toolBar);
        img_ToolBarAdd = (ImageButton) toolbar.findViewById(R.id.imgAdd);
        img_ToolBarAdd.setVisibility(View.GONE);
        tabLayout = (TabLayout)view.findViewById(R.id.startBetChat_TabLayout);
        viewpager = (ViewPager)view.findViewById(R.id.startBetChat_ViewPager);
    }
    private void initTab() {
        viewpager.setAdapter(new StartBetChatPagerAdapater(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewpager);
        viewpager.setOffscreenPageLimit(2);
        tabLayout.setClickable(true);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }





    public class StartBetChatPagerAdapater extends FragmentPagerAdapter{
        private String tabTitles[] = getResources().getStringArray(R.array.startBetChat);

        public StartBetChatPagerAdapater(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position)
            {
                case 0:
                    return new PrivateBetfragment();
                case 1:
                    return new PrivateChatFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }
}
