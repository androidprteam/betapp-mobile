package com.android.betapp.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.betapp.R;
import com.android.betapp.activities.Country_Email_verifyActivity;
import com.android.betapp.activities.WelcomeScreenActivity;
import com.android.betapp.utils.PreferenceUtil;

/**
 * Created by star on 7/21/2016.
 */
public class HomeFragment extends BaseFragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;


    private int[] tabIcons = {
            R.drawable.ic_profile,
            R.drawable.ic_home_history,
            R.drawable.ic_home_feed,
//            R.drawable.ic_home_settings

    };

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initVew(view);
        SharedPreferences sharedPref;
        sharedPref = PreferenceUtil.get(getActivity());
        String firstTimeLaunch = sharedPref.getString(PreferenceUtil.IS_FIRST_LAUCH, "");
        String userName = sharedPref.getString(PreferenceUtil.USER_NAME, "");
        if (userName.isEmpty()) {
            Intent in = new Intent(getActivity(), Country_Email_verifyActivity.class);
            startActivity(in);
            getActivity().finish();
        } else {

            if (firstTimeLaunch.isEmpty()) {
                Intent in = new Intent(getActivity(), WelcomeScreenActivity.class);
                startActivity(in);

                SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                PREF.putString(PreferenceUtil.IS_FIRST_LAUCH, "launched");
                PREF.apply();
                getActivity().finish();
            } else {
                initListener();
                initTab();
            }
        }
        return view;
    }


    private void initVew(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.home_viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.home_tabLayout);
    }

    private void initListener() {
    }

    private void initTab() {
        viewPager.setAdapter(new NavigationPageAdapter(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);
        setupTabIcons();
//        tabLayout.setClickable(true);

//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

    }

    private void setupTabIcons() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            LinearLayout relativeLayout = (LinearLayout)
                    LayoutInflater.from(getActivity()).inflate(R.layout.home_custome_tab, tabLayout, false);

            ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.ivTabIcon);
            imageView.setImageResource(tabIcons[i]);
            tab.setCustomView(relativeLayout);
//            tab.select();
        }

    }


    public class NavigationPageAdapter extends FragmentPagerAdapter {


        public NavigationPageAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {

                case 0:
                    return new ProfileFragment();
                case 1:
                    return new OnlineFragment();
                case 2:
                    return new FeedFragment();
//                case 3:
//                    return new SettingsFragment();

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

    }
}
