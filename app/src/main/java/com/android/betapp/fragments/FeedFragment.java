package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.betapp.R;

/**
 * Created by star on 8/2/2016.
 */
public class FeedFragment extends BaseFragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;

    private String tabname[];
    Toolbar toolbar;
    ImageButton imgAddToolBar;

    public static FeedFragment newInstance() {
        Bundle args = new Bundle();
        FeedFragment fragment = new FeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        tabname = new String[]{getResources().getString(R.string.txt_tab_feed), getResources().getString(R.string.txt_tab_bets)};
        initview(view);
        initlistener();
        initTab();
        setHasOptionsMenu(false);
        return view;
    }

    private void initlistener() {

    }

    private void initview(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.feed_viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.feed_tabLayout);
        toolbar = (Toolbar) view.findViewById(R.id.toolBar);
        imgAddToolBar = (ImageButton) toolbar.findViewById(R.id.imgAdd);
        imgAddToolBar.setVisibility(View.GONE);
    }

    private void initTab() {
        viewPager.setAdapter(new NavigationPageAdapter(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);
        setupTabIcons();
//        tabLayout.setClickable(true);

//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

    }

    private void setupTabIcons() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout)
                    LayoutInflater.from(getActivity()).inflate(R.layout.cutome_tab, tabLayout, false);
            View view = relativeLayout.findViewById(R.id.viewDevider);
            TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tab_title);
            if (i == 0) {
                view.setVisibility(View.GONE);
            }
            tabTextView.setText(tabname[i]);
            tab.setCustomView(relativeLayout);
//            tab.select();
        }

    }

    class NavigationPageAdapter extends FragmentPagerAdapter {


        public NavigationPageAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {

                case 0:
                    return new FeedTabFragment();
                case 1:
                    return new BetsTabFragment();

            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

    }


}
