package com.android.betapp.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.betapp.R;
import com.android.betapp.activities.CoinHistoryActivity;
import com.android.betapp.activities.ViewPagerActivity;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;

/**
 * Created by star on 7/24/2016.
 */
public class CoinsFragment extends BaseFragment implements View.OnClickListener {


    private LinearLayout llWalletCoins, llPoolCoin, llLastWin, llLastLost;
    private FrameLayout flTotalcoins;

    public static CoinsFragment newInstance() {
        CoinsFragment fragment = new CoinsFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coins, container, false);
        initView(view);

        return view;
    }

    private void initView(View view) {
        llWalletCoins = (LinearLayout) view.findViewById(R.id.llWalletCoins);
        llPoolCoin = (LinearLayout) view.findViewById(R.id.llPoolCoin);
//        llLastLost = (LinearLayout) view.findViewById(R.id.llLastLost);
//        llLastWin = (LinearLayout) view.findViewById(R.id.llLastWin);
        flTotalcoins = (FrameLayout) view.findViewById(R.id.flTotalcoins);
        DecoView decoView = (DecoView) view.findViewById(R.id.dynamicArcView);
        llWalletCoins.setOnClickListener(this);
        llPoolCoin.setOnClickListener(this);
//        llLastLost.setOnClickListener(this);
//        llLastWin.setOnClickListener(this);
        flTotalcoins.setOnClickListener(this);

        final SeriesItem WalletCoins = new SeriesItem.Builder(Color.parseColor("#4fd2c0"))
                .setRange(0, 100, 100)
                .build();

        final int backIndex = decoView.addSeries(WalletCoins);
        final SeriesItem poolCoins = new SeriesItem.Builder(Color.parseColor("#FF0000"))
                .setRange(0, 3233, 0)//minVAl,MaxVal,InitVal
                .build();

        int series1Index = decoView.addSeries(poolCoins);

        final TextView textPercentage = (TextView) view.findViewById(R.id.textPercentage);
        poolCoins.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
//                float percentFilled = ((currentPosition - poolCoins.getMinValue()) / (poolCoins.getMaxValue() - poolCoins.getMinValue()));
                textPercentage.setText(String.valueOf(currentPosition ));

            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        decoView.addEvent(new DecoEvent.Builder(3233).setIndex(backIndex).setDelay(0).build());
        decoView.addEvent(new DecoEvent.Builder(3000).setIndex(series1Index).setDelay(1000).build());
//        decoView.addEvent(new DecoEvent.Builder(30f).setIndex(series1Index).setDelay(9000).build());

    }

    @Override
    public void onClick(View view) {

        if (view == llWalletCoins) {
            startActivity(CoinHistoryActivity.getstartIntent(getActivity()));
        } else if (view == llPoolCoin) {
            startActivity(CoinHistoryActivity.getstartIntent(getActivity()));
        } else if (view == llLastLost) {
            startActivity(CoinHistoryActivity.getstartIntent(getActivity()));
        } else if (view == llLastWin) {
            startActivity(CoinHistoryActivity.getstartIntent(getActivity()));
        } else if (view == flTotalcoins) {
            startActivity(CoinHistoryActivity.getstartIntent(getActivity()));
//            startActivity(ViewPagerActivity.getstartIntent(getActivity()));
        }

    }
}
