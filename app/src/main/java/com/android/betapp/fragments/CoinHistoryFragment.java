package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.betapp.R;
import com.android.betapp.adapters.CoinHistoryAdapter;

/**
 * Created by star on 8/29/2016.
 */
public class CoinHistoryFragment extends BaseFragment {

    CoinHistoryAdapter adapter;
    private RecyclerView rvCoinHistory;
    private LinearLayoutManager layoutManager;

    public static CoinHistoryFragment newInstance() {
        Bundle args = new Bundle();
        CoinHistoryFragment fragment = new CoinHistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coint_history, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        rvCoinHistory = (RecyclerView) view.findViewById(R.id.rvCoinHistory);
        rvCoinHistory.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCoinHistory.setLayoutManager(layoutManager);
        adapter = new CoinHistoryAdapter(getActivity());
        rvCoinHistory.setAdapter(adapter);

    }

}
