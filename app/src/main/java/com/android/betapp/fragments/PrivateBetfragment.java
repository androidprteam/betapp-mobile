package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.betapp.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hp on 04-07-2017.
 */

public class PrivateBetfragment extends BaseFragment {
    TextView tvTimer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.private_bet_fragment, container, false);

        initView(view);
        long maxtime = System.currentTimeMillis() + 100;
        long outputTime = Math.abs(maxtime - System.currentTimeMillis());
        if (outputTime < 0) {
        } else {
            Date date = new java.util.Date(outputTime);
            String result = new SimpleDateFormat("hh:mm:ss").format(date);
            tvTimer.setText(result);
        }
        return view;
    }

    private void initView(View view) {
        tvTimer = (TextView) view.findViewById(R.id.tv_timer);
    }
}
