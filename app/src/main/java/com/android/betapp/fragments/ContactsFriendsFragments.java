package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.betapp.Events.ContactListTransferEvent;
import com.android.betapp.R;
import com.android.betapp.adapters.FriendsAdapter;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hp on 07-07-2017.
 */

public class ContactsFriendsFragments extends BaseFragment{
    RecyclerView rv_friends;
    ArrayList<JSONObject> array;
    ArrayList<HashMap<String, String>> addressList;
    private HashMap<String, String> addrerss;
    FriendsAdapter adpt;
    LinearLayoutManager layoutManager;

    public ContactsFriendsFragments(ArrayList<JSONObject> contacts) {
        array =contacts;
    }

    public ContactsFriendsFragments() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contacts_friends_fragment,container,false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        rv_friends = (RecyclerView)view.findViewById(R.id.rv_friends);
        JSONArray friendArray = new JSONArray(array);
        JSONObject contact;
        addressList = new ArrayList<HashMap<String, String>>();


        for (int i = 0 ; i<friendArray.length();i++)
        {
            try {
                addrerss = new HashMap<String,String>();
                contact=friendArray.getJSONObject(i);
                addrerss.put("name",contact.getString("name"));
                addrerss.put("number",contact.getString("number"));
                addressList.add(addrerss);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adpt =  new FriendsAdapter(getContext(),addressList);
        rv_friends.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rv_friends.setLayoutManager(layoutManager);
        adpt.notifyDataSetChanged();
        rv_friends.setAdapter(adpt);

    }

    @Subscribe
    public void getContacts(ContactListTransferEvent event){
        array = event.getArrayValue();
    }
}
