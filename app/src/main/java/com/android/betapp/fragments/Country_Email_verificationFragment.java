package com.android.betapp.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.betapp.R;
import com.android.betapp.activities.BaseActivity;
import com.android.betapp.activities.HomeActivity;
import com.android.betapp.apis.UpdateProfileService;
import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.ResultCode;
import com.android.betapp.apis.requests.UpdateProfileRequest;
import com.android.betapp.apis.response.UpdateProfileResponce;
import com.android.betapp.utils.ImageUtil;
import com.android.betapp.utils.PermissionUtils;
import com.android.betapp.utils.PreferenceUtil;
import com.iceteck.silicompressorr.SiliCompressor;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

/**
 * Created by star on 10/3/2016.
 */
public class Country_Email_verificationFragment extends BaseFragment {

    private static final String ARG_COUNTRNAME = "ARG_COUNTRNAME";
    private Button btnext;
    private EditText etFirstName, etLastName, etCityName, etEmail, etCountry, etMobilNumber, etCountryCode;
    private ImageView img_Upload;
    private CircularImageView cirImgProfile;
    Bitmap bitmap;
    String imageNameToSend;


    public static Country_Email_verificationFragment newInstance(String country_name) {
        Country_Email_verificationFragment fragment = new Country_Email_verificationFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_COUNTRNAME, country_name);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_email_country_varification, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void initView(View view) {
        btnext = (Button) view.findViewById(R.id.btnext);
        etFirstName = (EditText) view.findViewById(R.id.etFirstName);
        etLastName = (EditText) view.findViewById(R.id.etLastName);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etCityName = (EditText) view.findViewById(R.id.etCityName);
        etCountry = (EditText) view.findViewById(R.id.et_Contryname);
        etCountryCode = (EditText) view.findViewById(R.id.et_countryCode);
        etMobilNumber = (EditText) view.findViewById(R.id.et_mobileNumber);
        img_Upload = (ImageView) view.findViewById(R.id.img_upload);
        cirImgProfile = (CircularImageView)view.findViewById(R.id.cir_img_Profile);

        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    etFirstName.setError(null);
                }
            }
        });
        etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    etLastName.setError(null);
                }
            }
        });
        etCityName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    etCityName.setError(null);
                }
            }
        });
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    if (!validateEmailAddress(etEmail.getText().toString().trim())) {
                        etEmail.setError(getResources().getString(R.string.txt_validatio_email));
                    } else {
                        etEmail.setError(null);
                    }
                }
            }
        });

        btnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();
            }
        });
        img_Upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(PermissionUtils.isCameraPermissionGranted(getContext(),getActivity()))
               {
                   captureImageDialog();
               }
               else {
                   Toast.makeText(getContext(), "Not granted", Toast.LENGTH_SHORT).show();

               }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:

                if (resultCode == RESULT_OK) {
                    bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    cirImgProfile.setImageBitmap(bitmap);
                    imageNameToSend = ImageUtil.convert(bitmap);

                }

                break;
            case 1:

                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String tempImageUrl= String.valueOf(selectedImage);
                    try {
                        bitmap = SiliCompressor.with(getContext()).getCompressBitmap(tempImageUrl);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    cirImgProfile.setImageBitmap(bitmap);
                    imageNameToSend = ImageUtil.convert(bitmap);
                }
                break;
        }

    }

    private void captureImageDialog() {
        final Dialog dialog = new Dialog(getActivity());

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        DisplayMetrics display = getActivity().getResources().getDisplayMetrics();
        dialog.setContentView(R.layout.dialog_image_capture);
        TextView btn_takePhoto = (TextView) dialog.findViewById(R.id.btn_takePhoto);
        TextView btn_captureImage = (TextView) dialog.findViewById(R.id.btn_captureImage);
        btn_takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);
                dialog.cancel();
            }
        });
        btn_captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(PermissionUtils.isStoragePermissionGranted(getContext(),getActivity())) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);
                    dialog.cancel();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void validation() {

        if (etFirstName.getText().toString().trim().equals("") || etLastName.getText().toString().trim().equals("")
                || etCityName.getText().toString().trim().equals("") || etEmail.getText().toString().trim().equals("") || etCountry.getText().toString().trim().equals("") || etCountryCode.getText().toString().trim().equals("") || etMobilNumber.getText().toString().trim().equals("")) {
            etFirstName.setError(getResources().getString(R.string.txt_validatio_null));
            etLastName.setError(getResources().getString(R.string.txt_validatio_null));
            etCityName.setError(getResources().getString(R.string.txt_validatio_null));
            etEmail.setError(getResources().getString(R.string.txt_validatio_null));
            etCountry.setError(getResources().getString(R.string.txt_validatio_null));
            etCountryCode.setError(getResources().getString(R.string.txt_validatio_null));
            etMobilNumber.setError(getResources().getString(R.string.txt_validatio_null));

        } else if (etFirstName.getText().toString().trim().length() > 3 & etLastName.getText().toString().trim().length() > 0
                & etCityName.getText().toString().trim().length() > 3
                & !etCountry.getText().toString().trim().equals("") & etCountryCode.getText().toString().trim().length() >=2 & etMobilNumber.getText().toString().trim().length() >=10) {

            if (validateEmailAddress(etEmail.getText().toString().trim())) {
//                insertinguserDetails();
                SharedPreferences.Editor PREF =PreferenceUtil.edit(getContext());
                PREF.putString(PreferenceUtil.USER_NAME,etFirstName.getText().toString().trim());
                PREF.apply();

                startActivity(HomeActivity.getstartIntent(getActivity()));
                Toast.makeText(getContext(), "Registered", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (etFirstName.getText().toString().trim().length() < 3) {
                etFirstName.setError(getResources().getString(R.string.txt_Validation_Name));
            }
            if (etLastName.getText().toString().trim().equals("")) {
                etLastName.setError(getResources().getString(R.string.txt_validatio_null));
            }
            if (etCityName.getText().toString().trim().equals("")) {
                etCityName.setError(getResources().getString(R.string.txt_validatio_null));
            }
            if (etEmail.getText().toString().trim().equals("")) {
                etEmail.setError(getResources().getString(R.string.txt_validatio_null));
            }
            if (etMobilNumber.getText().toString().trim().length() < 10) {
                etMobilNumber.setError(getResources().getString(R.string.txt_Validation_Mobile));
            }
        }
    }

    public boolean validateEmailAddress(String username) {
        String expression = "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = username;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }


    public void insertinguserDetails() {
        UpdateProfileService service = new UpdateProfileService(getActivity());
        UpdateProfileRequest request = new UpdateProfileRequest();
        request.setUserid(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_ID, null));
        request.setFirst_name(etFirstName.getText().toString().trim());
        request.setLast_name(etLastName.getText().toString().trim());
        request.setUser_city(etCityName.getText().toString().trim());
        request.setUser_country(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COUNTRY_ID, null));
        service.post(request, new ApiResponse<UpdateProfileResponce>() {
            @Override
            public void onSuccess(ResultCode resultCode, UpdateProfileResponce response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                startActivity(HomeActivity.getstartIntent(getActivity()));
            }

            @Override
            public void onError(ResultCode resultCode, UpdateProfileResponce response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
