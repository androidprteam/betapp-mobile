package com.android.betapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.betapp.Events.ContactListTransferEvent;
import com.android.betapp.Events.StartBetChatEvent;
import com.android.betapp.R;
import com.android.betapp.activities.ContactListActivity;
import com.android.betapp.adapters.OnlineAdapter;
import com.android.betapp.bus.BusFactory;
import com.android.betapp.utils.PermissionUtils;
import com.squareup.otto.Subscribe;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by star on 8/2/2016.
 */
public class OnlineFragment extends BaseFragment {


    private RecyclerView rvOnline;
    private LinearLayoutManager layoutManager;
    private OnlineAdapter betsAdapter;
    MenuItem menuItem;
   final private int PICK_CONTACT = 1;
    Toolbar toolbar;
    ImageButton imgAddToolBar;


    public static OnlineFragment newInstance() {
        Bundle args = new Bundle();
        OnlineFragment fragment = new OnlineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_online,container,false);
        initview(view);
        initlistener();
        setadapter();
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        menuItem = menu.add("action");
//        menuItem.setTitle(" ");
//        menuItem.setIcon(R.drawable.ic_addfriend);
//        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Toast.makeText(getContext(), "Toasted", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//        startActivityForResult(intent, PICK_CONTACT);
        if(PermissionUtils.isContactPermissionGranted(getContext(),getActivity())) {
            Intent in = new Intent(getActivity(), ContactListActivity.class);
            startActivity(in);
        }
        else
        {
            Toast.makeText(getContext(), "Not granted", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);

    }

    private void initlistener() {

    }

    private void initview(View view) {
        rvOnline = (RecyclerView) view.findViewById(R.id.rvOnline);
        toolbar = (Toolbar)view.findViewById(R.id.toolBar) ;
        imgAddToolBar = (ImageButton) toolbar.findViewById(R.id.imgAdd);
        imgAddToolBar.setVisibility(View.VISIBLE);
        imgAddToolBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Wow", Toast.LENGTH_SHORT).show();
                if(PermissionUtils.isContactPermissionGranted(getContext(),getActivity()))
                {
                    getContectsFromPhone();
                }

            }
        });
    }
    private void setadapter() {
        rvOnline.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rvOnline.setLayoutManager(layoutManager);
        betsAdapter = new OnlineAdapter(getActivity());
        rvOnline.setAdapter(betsAdapter);
        betsAdapter.notifyDataSetChanged();

    }

    public void storeContactInArray() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case PICK_CONTACT:
                if(resultCode== Activity.RESULT_OK)
                {

                }
                break;
            default:
                Toast.makeText(getContext(), "PLease try again later", Toast.LENGTH_SHORT).show();

        }
    }
    @Subscribe
    public void getStartbetChatEvent(StartBetChatEvent event)
    {
        StartBetChatFragment fragment =  new StartBetChatFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public void getContectsFromPhone(){
        ProgressBar progressBar= new ProgressBar(getContext());
        ArrayList<JSONObject> contacts = new ArrayList<>();
        //to store name-number pair


        try {
            Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

            while (phones.moveToNext()) {
                String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                JSONObject obj = new JSONObject();
                obj.put("name", name);
                obj.put("number",phoneNumber);
                contacts.add(obj);
            }
            phones.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("contu"+contacts);
        System.out.println("contu Size"+contacts.size());
        ContactsFriendsFragments contactsFriendsFragments =  new ContactsFriendsFragments(contacts);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container,contactsFriendsFragments);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
