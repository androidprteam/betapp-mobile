package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.betapp.R;
import com.android.betapp.adapters.BetsAdapter;
import com.android.betapp.adapters.TabFeedAdapter;

/**
 * Created by star on 8/27/2016.
 */
public class FeedTabFragment extends BaseFragment {

    private RecyclerView rvFeed;
    private LinearLayoutManager layoutManager;
    private TabFeedAdapter tabFeedAdapter;


    public static FeedTabFragment newInstance() {
        Bundle args = new Bundle();
        FeedTabFragment fragment = new FeedTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_tab_feed,container,false);
        initView(view);
        iniLestener();
        setadapter();
        return view;
    }

    private void iniLestener() {

    }

    public void initView(View view) {
        rvFeed = (RecyclerView) view.findViewById(R.id.rvFeed);
    }


    private void setadapter() {
        rvFeed.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rvFeed.setLayoutManager(layoutManager);
        tabFeedAdapter = new TabFeedAdapter(getActivity());
        rvFeed.setAdapter(tabFeedAdapter);

    }

}
