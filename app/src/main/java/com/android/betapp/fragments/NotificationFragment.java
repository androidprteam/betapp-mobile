package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.betapp.R;
import com.android.betapp.adapters.NotificationAdapter;

/**
 * Created by star on 7/24/2016.
 */
public class NotificationFragment extends BaseFragment {

    private RecyclerView rvNotification;
    private LinearLayoutManager layoutManager;
    private NotificationAdapter notificationAdapter;

    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        initView(view);
        initLestener();
        setadpter();
        return view;

    }

    private void setadpter() {
        rvNotification.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvNotification.setLayoutManager(layoutManager);
        notificationAdapter=new NotificationAdapter(getActivity());
        rvNotification.setAdapter(notificationAdapter);

    }

    public void initView(View view) {
        rvNotification = (RecyclerView) view.findViewById(R.id.rvNotification);

    }

    private void initLestener() {

    }


}
