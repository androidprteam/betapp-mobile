package com.android.betapp.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.betapp.R;
import com.android.betapp.adapters.BetsAdapter;
import com.android.betapp.utils.DividerItemDecoration;

/**
 * Created by star on 7/24/2016.
 */
public class BetFragment extends BaseFragment {

    private RecyclerView rvBets;
    private LinearLayoutManager layoutManager;
    private BetsAdapter betsAdapter;

    public static BetFragment newInstance() {
        BetFragment fragment = new BetFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bets, container, false);
        initView(view);
        iniLestener();
        setadapter();


        return view;
    }

    public void initView(View view) {
        rvBets = (RecyclerView) view.findViewById(R.id.rvBet);
    }

    public void iniLestener() {
    }

    private void setadapter() {
        rvBets.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvBets.setLayoutManager(layoutManager);
        betsAdapter = new BetsAdapter(getActivity());
        rvBets.setAdapter(betsAdapter);

    }


}
