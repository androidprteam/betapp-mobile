package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.android.betapp.R;
import com.android.betapp.activities.Country_Email_verifyActivity;
import com.android.betapp.apis.VerifyOTPService;
import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.ResultCode;
import com.android.betapp.apis.requests.VerifyOTPRequest;
import com.android.betapp.apis.response.VerifyOTPResponce;

/**
 * Created by star on 7/24/2016.
 */
public class OTPFragment extends BaseFragment implements View.OnClickListener {


    private static final String ARG_MOBILE_NUMBER = "ARG_MOBILE_NUMBER";
    private static final String ARG_USER_ID = "ARG_USER_ID";
    private static final String ARG_COUNTRY_NAME = "ARG_COUNTRY_NAME";
    private static String country_name;
    private static String country_id;
    private static String mobilnumber;
    private String privacypolacy;
    private Button btnext;
    private EditText etOTP;
    private CheckBox cbAllowShare;

    public static OTPFragment newInstance(String mobilenumber, String country_name, String country_id) {
        OTPFragment fragment = new OTPFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_MOBILE_NUMBER, mobilenumber);
        bundle.putSerializable(ARG_COUNTRY_NAME, country_name);
        bundle.putSerializable(ARG_USER_ID, country_id);
        fragment.setArguments(bundle);
        return fragment;
    }
    //// OTP verification not working


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mobilnumber = (String) getArguments().getSerializable(ARG_MOBILE_NUMBER);
            country_name = (String) getArguments().getSerializable(ARG_COUNTRY_NAME);
            country_id = (String) getArguments().getSerializable(ARG_USER_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp, container, false);
        initView(view);
        initLiestener();
        return view;

    }


    public void initView(View view) {
        btnext = (Button) view.findViewById(R.id.btnext);
        etOTP = (EditText) view.findViewById(R.id.etOTP);
        cbAllowShare = (CheckBox) view.findViewById(R.id.cbAllowShare);
        cbAllowShare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    privacypolacy = "1";
                } else {
                    privacypolacy = "0";
                }

            }
        });
        etOTP.setText("12345");
    }

    public void initLiestener() {
        btnext.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btnext) {

            if (!etOTP.getText().toString().isEmpty()) {
                verifyOTP();
            } else {
                etOTP.setError("Field must not bee empty");
            }
        }
    }

    public void verifyOTP() {
        VerifyOTPService service = new VerifyOTPService(getActivity());
        VerifyOTPRequest request = new VerifyOTPRequest();
        request.setPhonenumber(mobilnumber);
        request.setOtp_number(etOTP.getText().toString());
        request.setUserid(country_id);
        request.setAgree(privacypolacy);
        service.post(request, new ApiResponse<VerifyOTPResponce>() {
            @Override
            public void onSuccess(ResultCode resultCode, VerifyOTPResponce response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                startActivity(Country_Email_verifyActivity.getstartIntent(getActivity(), country_name));
            }

            @Override
            public void onError(ResultCode resultCode, VerifyOTPResponce response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
