package com.android.betapp.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.betapp.R;
import com.android.betapp.utils.PreferenceUtil;

import org.w3c.dom.Text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by star on 7/24/2016.
 */
public class ProfileFragment extends BaseFragment {

    private ViewPager profile_viewpager;
    private TabLayout profile_tablayout;
    private Handler handler;
    AppBarLayout appBarLayout;
    private TextView tvUserName,tvPlace;
    Toolbar toolbar;
    ImageButton imgAddToolBar;


    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        initView(view);
        initListener();
        initTab();
        return view;
    }

    private void initView(View view) {
        profile_viewpager = (ViewPager) view.findViewById(R.id.profile_viewpager);
        profile_tablayout = (TabLayout) view.findViewById(R.id.profile_tablayout);
         appBarLayout = (AppBarLayout)view.findViewById(R.id.appBar);
        tvUserName = (TextView) view.findViewById(R.id.tv_UserName);
        tvPlace = (TextView)view.findViewById(R.id.tv_Place);
        toolbar = (Toolbar)view.findViewById(R.id.toolBar) ;
        imgAddToolBar = (ImageButton) toolbar.findViewById(R.id.imgAdd);
        imgAddToolBar.setVisibility(View.GONE);
        handler=new Handler();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                appBarLayout.setExpanded(false, true);
            }
        };
        handler.postDelayed(runnable, 1500);
        SharedPreferences sharedPref = PreferenceUtil.get(getActivity());
        tvUserName.setText(sharedPref.getString(PreferenceUtil.USER_NAME,""));
    }

    private void initListener() {
    }

    private void initTab() {
        profile_viewpager.setAdapter(new ProfileTabs(getChildFragmentManager()));
        profile_tablayout.setupWithViewPager(profile_viewpager);
        profile_viewpager.setOffscreenPageLimit(3);
        profile_tablayout.setClickable(true);

        profile_tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                profile_viewpager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }



    public class ProfileTabs extends FragmentPagerAdapter {
        private String tabTitles[] = getResources().getStringArray(R.array.profileTab);

        public ProfileTabs(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0:
                    return new CoinsFragment();
                case 1:
                    return new NotificationFragment();
//                case 2:
//                return new BetFragment();

            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }
    }


}
