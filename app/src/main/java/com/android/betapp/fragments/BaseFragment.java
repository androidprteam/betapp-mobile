package com.android.betapp.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.android.betapp.R;
import com.android.betapp.activities.BaseActivity;
import com.android.betapp.bus.BusFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sanif on 04-01-2016.
 */
public class BaseFragment extends Fragment {


    private boolean enableBackButton = true;

    private ProgressDialog pDialog;
    private TextView toolBarTitle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusFactory.getBus().register(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusFactory.getBus().unregister(this);
    }

    /**
     * This method is called to set the Toolbar with id 'toolbar' as ActionBar
     * Override this method to give custom implementation
     *
     * @param v: The container view
     */
    public void setupToolbar(View v) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolBar);
        if (toolbar != null) {
            toolBarTitle = (TextView) v.findViewById(R.id.tvTitle);
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            if (getTitle() != 0) {
                toolBarTitle.setText(getTitle());
            }
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);//for disabling default title shown in action bar
            if (enableBackButton) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            }
        }
    }

    /**
     * override this method to give title to toolBar
     *
     * @return
     */
    public
    @StringRes
    int getTitle() {
        return 0;
    }

    public ActionBar getSupportActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setEnableBackButton(boolean enableBackButton) {
        this.enableBackButton = enableBackButton;
    }

    public void pop() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    public void showLoading(String message) {
        ((BaseActivity) getActivity()).showLoading(message);
    }

    public void showLoading() {
        ((BaseActivity) getActivity()).showLoading();
    }

    public void hideLoading() {
        ((BaseActivity) getActivity()).hideLoading();
    }

    public void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public void SetDate(final EditText editText) {
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog toDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                String date = dateFormatter.format(newDate.getTime()).toString();
                editText.setText(date);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        toDatePickerDialog.show();


    }

    public boolean validateEmailAddress(String username) {
        String expression = "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = username;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches(); }


}
