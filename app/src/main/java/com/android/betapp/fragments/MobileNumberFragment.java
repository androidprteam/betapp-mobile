package com.android.betapp.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.betapp.R;
import com.android.betapp.activities.OTPActivity;
import com.android.betapp.adapters.SipinerCustomeAdapter;
import com.android.betapp.apis.CountryListService;
import com.android.betapp.apis.NumberVerificationService;
import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.ResultCode;
import com.android.betapp.apis.requests.CountryListRequest;
import com.android.betapp.apis.requests.NumberVerificationRequest;
import com.android.betapp.apis.response.CountryListResponse;
import com.android.betapp.apis.response.NumberVerificationResponce;
import com.android.betapp.model.Country;
import com.android.betapp.utils.PreferenceUtil;

import java.util.ArrayList;

/**
 * Created by star on 7/19/2016.
 */
public class MobileNumberFragment extends BaseFragment {

    Spinner spCountry;
    ArrayList<Country> countryArrayList;
    TextView tvCountryCode, etPhonenumber;
    private String country_ID, device_ID, countr_validation, country_name;


    public static MobileNumberFragment newInstance() {
        MobileNumberFragment fragment = new MobileNumberFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mobile_number, container, false);
        setEnableBackButton(false);
        tvCountryCode = (TextView) view.findViewById(R.id.tvCountryCode);
        etPhonenumber = (EditText) view.findViewById(R.id.etPhonenumber);
        spCountry = (Spinner) view.findViewById(R.id.spCountry);
        countryArrayList = new ArrayList<Country>();
        Button btNext = (Button) view.findViewById(R.id.btNext);
        device_ID = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);

//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.country, R.layout.item_spinner_list);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spCountry.setAdapter(adapter);


        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tvCountryCode.setText(countryArrayList.get(i).getPhonecode());
                countr_validation = countryArrayList.get(i).getPhonevalidation();
                country_name = countryArrayList.get(i).getName();
                SharedPreferences.Editor pref = PreferenceUtil.edit(getActivity());
                pref.putString(PreferenceUtil.USER_COUNTRY_ID, countryArrayList.get(i).getId());
                pref.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                tvCountryCode.setText(countryArrayList.get(0).getPhonecode());
                country_name = countryArrayList.get(0).getName();
            }
        });
        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etPhonenumber.getText().length() == Integer.parseInt(countr_validation)) {
                    verify_number(etPhonenumber.getText().toString(), device_ID, tvCountryCode.getText().toString(), country_name);
                } else {
                    etPhonenumber.setError("Minimum required " + countr_validation + " digit phone number");
                }

            }
        });
        etPhonenumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= Integer.parseInt(countr_validation)) {
                    etPhonenumber.setError(null);
                }
            }
        });

        getCountryList();
        return view;
    }


    public void getCountryList() {
        CountryListService service = new CountryListService(getActivity());
        CountryListRequest request = new CountryListRequest();
        service.post(request, new ApiResponse<CountryListResponse>() {
            @Override
            public void onSuccess(ResultCode resultCode, CountryListResponse response) {
                countryArrayList.addAll(response.getCountry());
                SipinerCustomeAdapter adapter = new SipinerCustomeAdapter(getActivity(), response.getCountry());
                spCountry.setAdapter(adapter);
                tvCountryCode.setText(response.getCountry().get(0).getPhonecode());
//                        tvCountryCode.setText(response.getCountry().get(0).getPhonecode());
//                for (Country country:response.getCountry()) {
//                    if (country.equals("INDIA")){
//                    }
//                }
            }

            @Override
            public void onError(ResultCode resultCode, CountryListResponse response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void verify_number(String phonrnumber, String deviceid, final String countryid, final String country_name) {
        NumberVerificationService service = new NumberVerificationService(getActivity());
        NumberVerificationRequest request = new NumberVerificationRequest();
        request.setCountry_id(countryid);
        request.setPhonenumber(phonrnumber);
        request.setDevice_token(deviceid);
        request.setDevice_type("android");

        service.post(request, new ApiResponse<NumberVerificationResponce>() {
            @Override
            public void onSuccess(ResultCode resultCode, NumberVerificationResponce response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity(), response.getUser_data().getMobileCountry(), Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor pref = PreferenceUtil.edit(getActivity());
                pref.putString(PreferenceUtil.USER_ID, response.getUser_data().getUserid());
                pref.apply();
                startActivity(OTPActivity.getstartIntent(getActivity(), etPhonenumber.getText().toString(), country_name, response.getUser_data().userid));
            }

            @Override
            public void onError(ResultCode resultCode, NumberVerificationResponce response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
