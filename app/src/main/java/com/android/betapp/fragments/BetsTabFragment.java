package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.betapp.R;

/**
 * Created by star on 8/27/2016.
 */
public class BetsTabFragment extends BaseFragment {

    public static BetsTabFragment newInstance() {
        Bundle args = new Bundle();
        BetsTabFragment fragment = new BetsTabFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_tab_bets,container,false);

        return view;
    }
}
