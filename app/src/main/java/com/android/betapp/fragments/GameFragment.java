package com.android.betapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.betapp.R;

/**
 * Created by star on 8/2/2016.
 */
public class GameFragment extends BaseFragment {
    public static GameFragment newInstance() {
        Bundle args = new Bundle();
        GameFragment fragment = new GameFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_game,container,false);
        initView(view);
        initlistener();
        return view;

    }

    private void initView(View view) {

    }

    private void initlistener() {

    }
}
