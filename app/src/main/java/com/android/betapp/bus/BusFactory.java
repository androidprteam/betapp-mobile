package com.android.betapp.bus;

import com.squareup.otto.Bus;

/**
 * Created by Sanif on 23-01-2016.
 */
public class BusFactory {

    public static final Bus bus = new Bus();

    public static Bus getBus(){
        return bus;
    }
}
