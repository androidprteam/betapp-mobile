package com.android.betapp.Events;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hp on 11-07-2017.
 */

public class ContactListTransferEvent {
    public ArrayList<JSONObject> getArrayValue() {
        return arrayValue;
    }

    ArrayList<JSONObject> arrayValue;
    public ContactListTransferEvent(ArrayList<JSONObject> array) {
        this.arrayValue=array;
    }

}
