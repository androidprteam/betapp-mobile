package com.android.betapp.Events;

/**
 * Created by hp on 04-07-2017.
 */

public class StartBetChatEvent {
    public String getPersonId() {
        return personId;
    }

    public String getBetId() {
        return betId;
    }

    String personId,betId;
    public StartBetChatEvent(String personId, String betId) {
        this.personId = personId;
        this.betId = betId;

    }

}
