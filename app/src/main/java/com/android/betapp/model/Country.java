package com.android.betapp.model;

/**
 * Created by star on 9/30/2016.
 */
public class Country {

    private String id;
    private String name;
    private  String nicename;
    private String iso;
    private String phonecode;
    private String phonevalidation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNicename() {
        return nicename;
    }

    public void setNicename(String nicename) {
        this.nicename = nicename;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getPhonecode() {
        return phonecode;
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }

    public String getPhonevalidation() {
        return phonevalidation;
    }

    public void setPhonevalidation(String phonevalidation) {
        this.phonevalidation = phonevalidation;
    }
}
