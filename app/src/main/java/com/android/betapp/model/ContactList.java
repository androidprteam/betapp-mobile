package com.android.betapp.model;

/**
 * Created by hp on 03-07-2017.
 */

public class ContactList {
        String myName = "";
        String myNumber = "";

        public String getName() {
            return myName;
        }

        public void setName(String name) {
            myName = name;
        }

        public String getPhoneNum() {
            return myNumber;
        }

        public void setPhoneNum(String number) {
            myNumber = number;
        }

}
