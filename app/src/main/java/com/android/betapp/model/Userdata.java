package com.android.betapp.model;

/**
 * Created by star on 10/2/2016.
 */
public class Userdata {
    public String userid;
    public String userConstant;
    public Object userEmail;
    public String userStatus;
    public String userMobile;
    public String mobileCountry;
    public String userStatusName;
    public String userStatusIcon;
    public String userStatusLabel;
    public String deviceId;
    public String deviceType;
    public String deviceToken;
    public String otpNumber;
    public String otpRequestTime;
    public String otpStatus;
    public String otpVerificationTime;
    public String deviceStatusName;
    public String deviceStatusIcon;
    public String deviceStatusLabel;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserConstant() {
        return userConstant;
    }

    public void setUserConstant(String userConstant) {
        this.userConstant = userConstant;
    }

    public Object getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(Object userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getMobileCountry() {
        return mobileCountry;
    }

    public void setMobileCountry(String mobileCountry) {
        this.mobileCountry = mobileCountry;
    }

    public String getUserStatusName() {
        return userStatusName;
    }

    public void setUserStatusName(String userStatusName) {
        this.userStatusName = userStatusName;
    }

    public String getUserStatusIcon() {
        return userStatusIcon;
    }

    public void setUserStatusIcon(String userStatusIcon) {
        this.userStatusIcon = userStatusIcon;
    }

    public String getUserStatusLabel() {
        return userStatusLabel;
    }

    public void setUserStatusLabel(String userStatusLabel) {
        this.userStatusLabel = userStatusLabel;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getOtpNumber() {
        return otpNumber;
    }

    public void setOtpNumber(String otpNumber) {
        this.otpNumber = otpNumber;
    }

    public String getOtpRequestTime() {
        return otpRequestTime;
    }

    public void setOtpRequestTime(String otpRequestTime) {
        this.otpRequestTime = otpRequestTime;
    }

    public String getOtpStatus() {
        return otpStatus;
    }

    public void setOtpStatus(String otpStatus) {
        this.otpStatus = otpStatus;
    }

    public String getOtpVerificationTime() {
        return otpVerificationTime;
    }

    public void setOtpVerificationTime(String otpVerificationTime) {
        this.otpVerificationTime = otpVerificationTime;
    }

    public String getDeviceStatusName() {
        return deviceStatusName;
    }

    public void setDeviceStatusName(String deviceStatusName) {
        this.deviceStatusName = deviceStatusName;
    }

    public String getDeviceStatusIcon() {
        return deviceStatusIcon;
    }

    public void setDeviceStatusIcon(String deviceStatusIcon) {
        this.deviceStatusIcon = deviceStatusIcon;
    }

    public String getDeviceStatusLabel() {
        return deviceStatusLabel;
    }

    public void setDeviceStatusLabel(String deviceStatusLabel) {
        this.deviceStatusLabel = deviceStatusLabel;
    }
}
