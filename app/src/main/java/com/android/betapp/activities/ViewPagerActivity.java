package com.android.betapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.android.betapp.R;
import com.android.betapp.fragments.ViewPagerFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by star on 9/3/2016.
 */
public class ViewPagerActivity extends BaseActivity {

    public static Intent getstartIntent(Context context){
        Intent intent=new Intent(context,ViewPagerActivity.class);

        return intent;
    }

    MyPageAdapter pageAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);
        List<Fragment> fragments = getFragments();
        pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
        ViewPager pager =
                (ViewPager) findViewById(R.id.viewpager);
        pager.setAdapter(pageAdapter);
    }


    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        fList.add(ViewPagerFragment.newInstance("Fragment 1"));
        fList.add(ViewPagerFragment.newInstance("Fragment 2"));
        fList.add(ViewPagerFragment.newInstance("Fragment 3"));
        return fList;
    }

    class MyPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }
}
