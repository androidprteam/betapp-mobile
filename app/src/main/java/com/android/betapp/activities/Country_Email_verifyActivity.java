package com.android.betapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.betapp.R;
import com.android.betapp.fragments.CoinHistoryFragment;
import com.android.betapp.fragments.Country_Email_verificationFragment;

/**
 * Created by star on 10/3/2016.
 */
public class Country_Email_verifyActivity extends BaseActivity {

    public static final String ARG_COUNTRY="ARG_COUNTRY";
    private  String country_name;

    public static Intent getstartIntent(Context context,String country_name) {
        Intent intent = new Intent(context, Country_Email_verifyActivity.class);
        intent.putExtra(ARG_COUNTRY,country_name);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_email_verification);
        country_name= (String) getIntent().getSerializableExtra(ARG_COUNTRY);

        if (savedInstanceState == null) {
            addFragment(Country_Email_verificationFragment.newInstance(country_name), false);
        }
    }
}
