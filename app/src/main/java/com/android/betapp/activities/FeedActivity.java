package com.android.betapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.betapp.R;
import com.android.betapp.fragments.FeedFragment;

/**
 * Created by star on 8/2/2016.
 */
public class FeedActivity extends BaseActivity {
    public static Intent getstarIntent(Context context){
        Intent intent=new Intent(context,FeedActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        if (savedInstanceState==null){
            addFragment(FeedFragment.newInstance(),false);
        }
    }
}
