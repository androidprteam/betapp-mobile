package com.android.betapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.betapp.R;
import com.android.betapp.fragments.OTPFragment;

/**
 * Created by star on 7/24/2016.
 */
public class OTPActivity extends BaseActivity {

    private static final String ARG_MOBILE_NUMBER = "ARG_MOBILE_NUMBER";
    private static final String ARG_country_name = "ARG_country_name";
    private static final String ARG_country_ID = "ARG_country_ID";
    private static  String country_name;
    private static String mobilnumber;
    private static String country_id;

    public static Intent getstartIntent(Context context, String mobilenumber,String country_name,String country_id) {
        Intent intent = new Intent(context, OTPActivity.class);
        intent.putExtra(ARG_MOBILE_NUMBER, mobilenumber);
        intent.putExtra(ARG_country_name, country_name);
        intent.putExtra(ARG_country_ID, country_id);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        mobilnumber = (String) getIntent().getSerializableExtra(ARG_MOBILE_NUMBER);
        country_name = (String) getIntent().getSerializableExtra(ARG_country_name);
        country_id = (String) getIntent().getSerializableExtra(ARG_country_ID);
        if (savedInstanceState == null) {
            addFragment(OTPFragment.newInstance(mobilnumber, country_name,country_id), false);
        }
    }
}
