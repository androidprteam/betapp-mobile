package com.android.betapp.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.betapp.R;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by hp on 30-06-2017.
 */

public class WelcomeScreenActivity extends BaseActivity {

        Button btn_Skip;
        ViewPager vp_WelcomePager;
        PagerAdapter pagerAdapter;
        int[] layouts;
        LayoutInflater layoutInflater;
        CircleIndicator viewPagerIndicator;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            // Making notification bar transparent
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }
            final Intent in = new Intent(this, HomeActivity.class);
            setContentView(R.layout.welcome_screen_activity);
            btn_Skip = (Button) findViewById(R.id.btn_skip);
            vp_WelcomePager = (ViewPager) findViewById(R.id.welcomePage_ViewPager);
            viewPagerIndicator = (CircleIndicator) findViewById(R.id.circleIndicator);
            layouts = new int[]{R.layout.welcome_screen1, R.layout.welcome_screen2, R.layout.welcome_screen3};
            pagerAdapter = new PagerAdapter() {
                @Override
                public int getCount() {
                    return layouts.length;
                }

                @Override
                public boolean isViewFromObject(View view, Object object) {
                    return view == object;
                }

                @Override
                public Object instantiateItem(ViewGroup container, int position) {
                    layoutInflater = (LayoutInflater) getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
                    View view = layoutInflater.inflate(layouts[position], container, false);
                    container.addView(view);

                    return view;
                }

                @Override
                public void destroyItem(ViewGroup container, int position, Object object) {
//                super.destroyItem(container, position, object);
                    View view = (View) object;
                    container.removeView(view);

                }
            };
            vp_WelcomePager.setAdapter(pagerAdapter);
            viewPagerIndicator.setViewPager(vp_WelcomePager);
            vp_WelcomePager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (position==2)
                    {
                        btn_Skip.setText("Go!");
                        btn_Skip.setTextColor(getResources().getColor(R.color.sky_Blue));
                        btn_Skip.setTextSize(20.0f);
                    }
                    else {
                        btn_Skip.setText("Skip");
                        btn_Skip.setTextColor(getResources().getColor(R.color.white));
                        btn_Skip.setTextSize(15.0f);
                    }

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            btn_Skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(in);
                    finish();
                }
            });
        }

        @Override
        public void onBackPressed() {
//        super.onBackPressed();
        }
    }
