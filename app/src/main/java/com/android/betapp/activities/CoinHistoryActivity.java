package com.android.betapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.betapp.R;
import com.android.betapp.fragments.CoinHistoryFragment;

/**
 * Created by star on 8/30/2016.
 */
public class CoinHistoryActivity extends BaseActivity {

    public static Intent getstartIntent(Context context) {
        Intent intent = new Intent(context, CoinHistoryActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin_history);
        if (savedInstanceState == null) {
            addFragment(CoinHistoryFragment.newInstance(), false);
        }
    }
}
