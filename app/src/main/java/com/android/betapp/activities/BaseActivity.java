package com.android.betapp.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;


import com.android.betapp.R;
import com.android.betapp.bus.BusFactory;
import com.android.betapp.utils.PermissionUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sanif on 03-01-2016.
 */
public class BaseActivity extends AppCompatActivity {

    private PermissionListener permissionListner;

    public interface PermissionListener {
        boolean onPermissionGranted(@Permission int permissionGroup);

        boolean onPermissionDenied(@Permission int permissionGroup);

        boolean doRationale(@Permission int permissionGroup);
    }

    public void setPermissionListener(PermissionListener permissionListener) {
        this.permissionListner = permissionListener;
    }

    public interface OnBackPressedListener {
        boolean onBackPressed();
    }

    private OnBackPressedListener backPressedListener;

    public static final int CALENDER_PERMISSION = 123;
    public static final int LOCATION_PERMISSION = 124;
    public static final int CAMERA_PERMISSION = 125;
    public static final int PHONE_PERMISSION = 126;
    public static final int CONTACTS_PERMISSION = 127;

    public static Map<Integer, String> permissionGroups = new HashMap<>();

    static {
        permissionGroups.put(CALENDER_PERMISSION, Manifest.permission.READ_CALENDAR);
        permissionGroups.put(LOCATION_PERMISSION, Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionGroups.put(CAMERA_PERMISSION, Manifest.permission.CAMERA);
        permissionGroups.put(PHONE_PERMISSION, Manifest.permission.READ_PHONE_STATE);
        permissionGroups.put(CONTACTS_PERMISSION, Manifest.permission.READ_CONTACTS);
    }

    //TODO: handle other permission groups

    private Toolbar mToolBar;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BusFactory.getBus().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusFactory.getBus().unregister(this);
    }

    public Toolbar getToolBar() {
        return mToolBar;
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mToolBar = (Toolbar) findViewById(R.id.toolBar);
        if (mToolBar != null)
            setSupportActionBar(mToolBar);
    }

    public void setActionBarTransparent() {
        if (mToolBar != null) {
            mToolBar.setBackgroundColor(Color.argb(255, 0, 0, 0));
        }
    }

    public void setOnBackPressedListener(OnBackPressedListener listener) {
        backPressedListener = listener;
    }

    /**
     * Override this method to handle the back stack callbacks.
     */
    public void onBackStackChanged() {

    }

    /**
     * Method to add fragment to the container.
     *
     * @param containerId:    The id of the container.
     * @param fragment:       The fragment to add.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void addFragment(int containerId, Fragment fragment, boolean addToBackStack) {

        if (isCurrentFragment(fragment)) {//no need to add a fragment twice
            return;
        }

        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        fTransaction.add(containerId, fragment, fragment.getClass().getSimpleName());//Tag : used for findFragmentByTag
        if (addToBackStack) {
            fTransaction.addToBackStack(null);
        }
        fTransaction.commit();
    }

    /**
     * Method to add fragment to the container with id R.id.container.
     *
     * @param fragment:       The fragment to add.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void addFragment(Fragment fragment, boolean addToBackStack) {
        addFragment(R.id.container, fragment, addToBackStack);
    }

    /**
     * Method to replace fragment in a container.
     *
     * @param containerId:    The id of the container.
     * @param fragment:       The fragment to replace.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void replaceFragment(int containerId, Fragment fragment, boolean addToBackStack, boolean animate) {

        if (isCurrentFragment(fragment)) {//no need to add a fragment twice
            return;
        }

        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        if (animate) {
//            fTransaction.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_right_out, R.anim.slide_right_in, R.anim.slide_left_out);
        }
        fTransaction.replace(containerId, fragment, fragment.getClass().getSimpleName());//Tag : used for findFragmentByTag
        if (addToBackStack) {
            fTransaction.addToBackStack(null);
        }
        fTransaction.commit();

    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        replaceFragment(R.id.container, fragment, addToBackStack, true);
    }

    public Fragment getFragment(Class clazz) {
        return getSupportFragmentManager().findFragmentByTag(clazz.getSimpleName());
    }

    public void popBackStack() {
        getSupportFragmentManager().popBackStack();
    }

    /**
     * To check whether the given fragment is the current fragment.
     *
     * @param fragment
     * @return
     */
    public boolean isCurrentFragment(Fragment fragment) {
        Fragment tFragment = getFragment(fragment.getClass());
        if (tFragment != null && tFragment.isVisible()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //to handel the actionbar back button
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showLoading(String message) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(this);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.setMessage(message);
        }
        pDialog.show();
    }

    public void showLoading() {
        showLoading("Loading..");
    }

    public void hideLoading() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }


    /***********************************************************************************************
     * Marshmallow permission checks
     **********************************************************************************8***********/

    /**
     * @param permissionGroup @CALENDER_PERMISSION,@LOCATION_PERMISSION,@CAMERA_PERMISSION,@PHONE_PERMISSION,@CONTACTS_PERMISSION
     * @return
     */
    public void obtainPermission(@Permission int permissionGroup) {
        if (isPermissionGranted(permissionGroup)) {
            onPermissionGranted(permissionGroup);
        } else {
            if (shouldRequestRationale(permissionGroup)) {
                doRationale(permissionGroup);
            } else {
                //permission has not been granted yet. Request it directly.
                getPermission(permissionGroup);
            }
        }
    }

    /**
     * used to check whether the permission is granted for the given permission group
     *
     * @param permissionGroup
     * @return
     */
    public boolean isPermissionGranted(@Permission int permissionGroup) {

        return PermissionUtils.isPermissionGranted(this, permissionGroups.get(permissionGroup));

    }

    public boolean shouldRequestRationale(@Permission int permissionGroup) {
        // Provide an additional rationale to the user if the permission was not granted
        // and the user would benefit from additional context for the use of the permission.
        // For example if the user has previously denied the permission.
        return ActivityCompat.shouldShowRequestPermissionRationale(this, permissionGroups.get(permissionGroup));
    }


    /**
     * override this method to give custom rationale
     *
     * @param permissionGroup
     */
    public void doRationale(@Permission final int permissionGroup) {
        if (permissionListner == null || permissionListner.doRationale(permissionGroup)) {
            Snackbar.make(findViewById(android.R.id.content), "Please grant us the permission to serve you better.",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getPermission(permissionGroup);
                        }
                    })
                    .show();
        }
    }

    public void getPermission(@Permission int permissionGroup) {
        ActivityCompat.requestPermissions(this, new String[]{permissionGroups.get(permissionGroup)},
                permissionGroup);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPermissionGranted(requestCode);
            // permission was granted, yay!

        } else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            onPermissionDenied(requestCode);
        }


    }


    /**
     * override this method to handle the cases when the permission is granted
     *
     * @param permissionGroup
     */
    public void onPermissionGranted(@Permission int permissionGroup) {
        if (permissionListner == null || permissionListner.onPermissionGranted(permissionGroup)) {

        }
    }

    /**
     * override this method to handle the cases when the permission is denied
     *
     * @param permissionGroup
     */
    public void onPermissionDenied(@Permission int permissionGroup) {
        if (permissionListner == null || permissionListner.onPermissionDenied(permissionGroup)) {

        }
    }

    @IntDef({CALENDER_PERMISSION, LOCATION_PERMISSION, CAMERA_PERMISSION, PHONE_PERMISSION, CONTACTS_PERMISSION})
//TODO: update with remaining permissions
    @Retention(RetentionPolicy.SOURCE)
    public @interface Permission {
    }

    //Nested fragments backstack workaround
    private boolean onBackPressed(FragmentManager fm) {
        if (fm != null) {
            if (fm.getBackStackEntryCount() > 0) {
                fm.popBackStack();
                return true;
            }

            List<Fragment> fragList = fm.getFragments();
            if (fragList != null && fragList.size() > 0) {
                for (Fragment frag : fragList) {
                    if (frag == null) {
                        continue;
                    }
                    if (frag.isVisible()) {
                        if (onBackPressed(frag.getChildFragmentManager())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (backPressedListener == null || !backPressedListener.onBackPressed()) {

            FragmentManager fm = getSupportFragmentManager();
            if (onBackPressed(fm)) {
                return;
            }
            super.onBackPressed();
        }
    }
}
