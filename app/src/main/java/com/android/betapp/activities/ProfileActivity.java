package com.android.betapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.betapp.R;
import com.android.betapp.fragments.HomeFragment;
import com.android.betapp.fragments.ProfileFragment;

/**
 * Created by star on 7/24/2016.
 */
public class ProfileActivity extends BaseActivity {

    public static Intent getstartIntent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profle);
        if (savedInstanceState == null) {
            addFragment(ProfileFragment.newInstance(), false);
        }
    }
}

