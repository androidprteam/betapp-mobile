package com.android.betapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.betapp.R;
import com.android.betapp.fragments.OnlineFragment;

/**
 * Created by star on 8/2/2016.
 */
public class OnlineActivity extends BaseActivity {
    public static Intent getstartIntent(Context context){
        Intent intent=new Intent(context,OnlineActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online);
        if (savedInstanceState==null){
            addFragment(OnlineFragment.newInstance(),false);
        }
    }
}
