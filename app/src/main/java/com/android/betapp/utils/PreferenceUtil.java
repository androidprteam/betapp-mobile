package com.android.betapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.security.PublicKey;

/**
 * Created by Sanif on 11-02-2016.
 * Usage:
 * PreferenceUtil.edit(getActivity()).putString(PreferenceUtil.EMAIL, "sanifss@gmail.com").commit();
 * OR
 * SharedPreferences.Editor editor = PreferenceUtil.edit(getActivity());
 * editor.putString(PreferenceUtil.EMAIL, "sanifss@gmail.com");
 * editor.putBoolean(PreferenceUtil.IS_LOGGED_IN, true);
 * editor.commit();
 */
public class PreferenceUtil {
    //PREFERENCES
    public static final String PREF_FILE = "BET_APP_PREF";
    public static final String  USER_ID="USER_ID";
    public static final String  USER_COUNTRY_ID="USER_COUNTRY_ID";
    public static final String IS_FIRST_LAUCH ="FIRST_TIME";
    public static final String USER_NAME = "USER_Name";

    //gallery permission


    public static SharedPreferences.Editor edit(Context context) {
        return get(context).edit();
    }

    public static SharedPreferences get(Context context) {
        return context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
    }

    //usage
//    PreferenceUtil.edit(getActivity()).putString(PreferenceUtil.EMAIL, "sanifss@gmail.com").commit();


}
