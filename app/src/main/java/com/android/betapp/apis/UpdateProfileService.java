package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.core.URL;
import com.android.betapp.apis.requests.NumberVerificationRequest;
import com.android.betapp.apis.requests.UpdateProfileRequest;
import com.android.betapp.apis.response.NumberVerificationResponce;
import com.android.betapp.apis.response.UpdateProfileResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class UpdateProfileService extends BaseService<UpdateProfileResponce> {
    public UpdateProfileService(Context context) {
        super(context);
    }
    public String URL= com.android.betapp.apis.core.URL.UpdateProfile;

    public void post(UpdateProfileRequest args, ApiResponse<UpdateProfileResponce> apiCallback) {
        super.post(URL, args, new TypeToken<UpdateProfileResponce>() {
        }, apiCallback);
    }
}
