package com.android.betapp.apis.response;

import com.android.betapp.model.Country;

import java.util.List;

/**
 * Created by star on 9/30/2016.
 */
public class CountryListResponse extends BaseResponse {

    private List<Country> country;

    public List<Country> getCountry() {
        return country;
    }

    public void setCountry(List<Country> country) {
        this.country = country;
    }
}
