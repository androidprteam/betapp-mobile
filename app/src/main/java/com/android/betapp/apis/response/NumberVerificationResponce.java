package com.android.betapp.apis.response;

import com.android.betapp.model.Userdata;

/**
 * Created by star on 9/30/2016.
 */
public class NumberVerificationResponce extends BaseResponse {

    private Userdata user_data;

    public Userdata getUser_data() {
        return user_data;
    }

    public void setUser_data(Userdata user_data) {
        this.user_data = user_data;
    }
}
