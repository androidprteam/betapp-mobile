package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.requests.NumberVerificationRequest;
import com.android.betapp.apis.response.NumberVerificationResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class NumberVerificationService extends BaseService<NumberVerificationResponce> {
    public String URL = com.android.betapp.apis.core.URL.NumberVerification;

    public NumberVerificationService(Context context) {
        super(context);
    }

    public void post(NumberVerificationRequest args, ApiResponse<NumberVerificationResponce> apiCallback) {
        super.post(URL, args, new TypeToken<NumberVerificationResponce>() {
        }, apiCallback);
    }
}
