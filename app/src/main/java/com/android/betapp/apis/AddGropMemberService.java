package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.requests.AddGroupMemberRequest;
import com.android.betapp.apis.response.AddGroupMemberResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class AddGropMemberService extends BaseService<AddGroupMemberResponce> {
    public String URL = com.android.betapp.apis.core.URL.AddGroupMember;

    public AddGropMemberService(Context context) {
        super(context);
    }

    public void post(AddGroupMemberRequest args, ApiResponse<AddGroupMemberResponce> apiCallback) {
        super.post(URL, args, new TypeToken<AddGroupMemberResponce>() {
        }, apiCallback);
    }
}
