package com.android.betapp.apis.core;


import com.android.betapp.apis.response.BaseResponse;

/**
 * Created by Sanif on 03-01-2016.
 */
public interface ApiResponse<T extends BaseResponse> {
    public void onSuccess(ResultCode resultCode, T response);

    public void onError(ResultCode resultCode,T response);

}

