package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.requests.UpdateContactRequest;
import com.android.betapp.apis.response.UpdateContactResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class UpdateContactService extends BaseService<UpdateContactResponce> {
    public String URL = com.android.betapp.apis.core.URL.UpdateContact;

    public UpdateContactService(Context context) {
        super(context);
    }

    public void post(UpdateContactRequest args, ApiResponse<UpdateContactResponce> apiCallback) {
        super.post(URL, args, new TypeToken<UpdateContactResponce>() {
        }, apiCallback);
    }

}
