package com.android.betapp.apis.requests;

/**
 * Created by bugs on 2/22/2016.
 */
public class BaseRequest {

    private String authToken;
    private String apiVersion;
    private String  appVersion;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }


    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}
