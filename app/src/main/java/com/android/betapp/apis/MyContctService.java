package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.requests.MyContactRequest;
import com.android.betapp.apis.response.MyContactResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class MyContctService extends BaseService<MyContactResponce> {
    public String URL = com.android.betapp.apis.core.URL.MyContact;

    public MyContctService(Context context) {
        super(context);
    }

    public void post(MyContactRequest args, ApiResponse<MyContactResponce> apiCallback) {
        super.post(URL, args, new TypeToken<MyContactResponce>() {
        }, apiCallback);
    }
}
