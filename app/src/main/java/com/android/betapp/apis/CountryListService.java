package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.requests.CountryListRequest;
import com.android.betapp.apis.response.CountryListResponse;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class CountryListService extends BaseService<CountryListResponse> {

    public String URL = com.android.betapp.apis.core.URL.CountryList;

    public CountryListService(Context context) {
        super(context);
    }

    public void post(CountryListRequest args, ApiResponse<CountryListResponse> apiCallback) {
        super.post(URL, args, new TypeToken<CountryListResponse>() {
        }, apiCallback);
    }

}
