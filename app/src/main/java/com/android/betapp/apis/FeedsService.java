package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.requests.FeedsRequest;
import com.android.betapp.apis.response.FeedsResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class FeedsService extends BaseService<FeedsResponce> {
    public String URL = com.android.betapp.apis.core.URL.Feeds;

    public FeedsService(Context context) {
        super(context);
    }

    public void post(FeedsRequest args, ApiResponse<FeedsResponce> apiCallback) {
        super.post(URL, args, new TypeToken<FeedsResponce>() {
        }, apiCallback);
    }
}
