package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.core.URL;
import com.android.betapp.apis.requests.NumberVerificationRequest;
import com.android.betapp.apis.requests.VerifyOTPRequest;
import com.android.betapp.apis.response.NumberVerificationResponce;
import com.android.betapp.apis.response.VerifyOTPResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class VerifyOTPService extends BaseService<VerifyOTPResponce> {
    public VerifyOTPService(Context context) {
        super(context);
    }
    public String URL= com.android.betapp.apis.core.URL.VerifyOTP;

    public void post(VerifyOTPRequest args, ApiResponse<VerifyOTPResponce> apiCallback) {
        super.post(URL, args, new TypeToken<VerifyOTPResponce>() {
        }, apiCallback);
    }
}
