package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.requests.UpdateProfileImageRequest;
import com.android.betapp.apis.response.UpdateProfileResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class UpdateProfileImageService extends BaseService<UpdateProfileResponce> {
    public String URL = com.android.betapp.apis.core.URL.UpdteProfileImage;

    public UpdateProfileImageService(Context context) {
        super(context);
    }

    public void post(UpdateProfileImageRequest args, ApiResponse<UpdateProfileResponce> apiCallback) {
        super.post(URL, args, new TypeToken<UpdateProfileResponce>() {
        }, apiCallback);
    }
}
