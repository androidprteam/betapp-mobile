package com.android.betapp.apis.response;


import com.android.betapp.apis.core.ResultCode;

/**
 * Created by bugs on 2/6/2016.
 */
public class BaseResponse {

    private ResultCode success;

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultCode getResultCode() {
        return success;
    }

    public void setResultCode(ResultCode success) {
        this.success = success;
    }

}
