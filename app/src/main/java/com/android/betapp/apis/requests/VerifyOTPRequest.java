package com.android.betapp.apis.requests;

/**
 * Created by star on 9/30/2016.
 */
public class VerifyOTPRequest extends BaseRequest {

    private String userid;
    private String otp_number;
    private String phonenumber;
    private String agree;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getOtp_number() {
        return otp_number;
    }

    public void setOtp_number(String otp_number) {
        this.otp_number = otp_number;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getAgree() {
        return agree;
    }

    public void setAgree(String agree) {
        this.agree = agree;
    }
}
