package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.requests.CreateGroupRequest;
import com.android.betapp.apis.response.CreateGroupResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class CreateGroupService extends BaseService<CreateGroupResponce> {
    public String URL = com.android.betapp.apis.core.URL.CreateGroup;

    public CreateGroupService(Context context) {
        super(context);
    }

    public void post(CreateGroupRequest args, ApiResponse<CreateGroupResponce> apiCallback) {
        super.post(URL, args, new TypeToken<CreateGroupResponce>() {
        }, apiCallback);
    }
}
