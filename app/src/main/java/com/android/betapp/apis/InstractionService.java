package com.android.betapp.apis;

import android.content.Context;

import com.android.betapp.apis.core.ApiResponse;
import com.android.betapp.apis.core.BaseService;
import com.android.betapp.apis.requests.InstractionRequest;
import com.android.betapp.apis.response.InstractionResponce;
import com.google.gson.reflect.TypeToken;

/**
 * Created by star on 9/30/2016.
 */
public class InstractionService extends BaseService<InstractionResponce> {
    public String URL = com.android.betapp.apis.core.URL.Instraction;

    public InstractionService(Context context) {
        super(context);
    }

    public void post(InstractionRequest args, ApiResponse<InstractionResponce> apiCallback) {
        super.post(URL, args, new TypeToken<InstractionResponce>() {
        }, apiCallback);
    }

}
