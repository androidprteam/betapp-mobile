package com.android.betapp.apis.core;

/**
 * Created by bugs on 2/6/2016.
 */
public class URL {
    public static final String BASE_URL = "http://52.66.91.61/index.php/apiv1/common/";

    public static final String CountryList = BASE_URL + "country";
    public static final String NumberVerification = BASE_URL + "register_mobile";
    public static final String VerifyOTP = BASE_URL + "verify_otp";
    public static final String UpdateProfile = BASE_URL + "update_porfile";
    public static final String Instraction = BASE_URL + "instructions";
    public static final String UpdteProfileImage = BASE_URL + "update_Profile_image";
    public static final String UpdateContact = BASE_URL + "update_contacts";
    public static final String MyContact = BASE_URL + "my_contacts";
    public static final String CreateGroup = BASE_URL + "create_group";
    public static final String AddGroupMember = BASE_URL + "add_group_member";
    public static final String Feeds = BASE_URL + "feeds";

}
