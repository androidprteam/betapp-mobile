package com.android.betapp.apis.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.android.betapp.BuildConfig;
import com.android.betapp.apis.requests.BaseRequest;
import com.android.betapp.apis.response.BaseResponse;
import com.android.betapp.utils.AppUtils;
import com.android.betapp.utils.PreferenceUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

/**
 * Created by Sanif on 03-01-2016.
 */
public abstract class BaseService<T extends BaseResponse> {

    final SharedPreferences preferences;
    private Context context;
    private String GET = "GET";
    private String POST = "POST";
    private FutureCallback<?> callback;

    public BaseService(Context context) {
        this.context = context;
        preferences = PreferenceUtil.get(context);
    }

    public static void rawResponse(Context context, String url, Object args) {
        Ion.with(context).load("POST", url).setJsonPojoBody(args).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("RAW", result);
            }
        });
    }

    protected void post(String url, BaseRequest args, TypeToken<T> returnType, final ApiResponse<T> apiCallback) {
        execute(POST, url, args, returnType, apiCallback);

    }

    protected void get(String url, BaseRequest args, TypeToken<T> returnType, final ApiResponse<T> apiCallback) {
        execute(GET, url, args, returnType, apiCallback);

    }

    protected synchronized void execute(String method, final String url, final BaseRequest args, final TypeToken<T> returnObjectType,
                                        final ApiResponse<T> apiCallback) {

        if (AppUtils.isNetworkAvailable(context)) {
//            if (args != null) {
//                args.setAuthToken("*db8ebetcoin+8B");
//                args.setAppVersion(BuildConfig.VERSION_NAME);
//                args.setApiVersion("1.0");
//                if (BuildConfig.DEBUG) Log.d("REQUEST", new Gson().toJson(args));
//            }

            Ion.with(context).load(method, url).setLogging("WebService", Log.DEBUG)
                    .addHeader("betcoin_app_version", "1")
                    .addHeader("betcoin_api_version", "1")
                    .addHeader("betcoin_api_key", "*db8ebetcoin+8B")
                    .setTimeout(60 * 60 * 1000)
                    .setJsonPojoBody(args)
                    .as(returnObjectType)
                    .withResponse()
                    .setCallback(new FutureCallback<Response<T>>() {
                        @Override
                        public void onCompleted(Exception e, Response<T> result) {
                            if (e != null) {
//                        Toast.makeText(context, "Exeption::"+e, Toast.LENGTH_SHORT).show();
                                Log.d("Exeption::", e + "");
                                e.printStackTrace();
                            }
                            if (result != null) {
                                if (BuildConfig.DEBUG) {
                                    Log.d("RESPONSE", new Gson().toJson(result.getResult()));
                                }
                                if (result.getResult() != null && result.getResult().getResultCode() == ResultCode.SUCCESS) {
                                    apiCallback.onSuccess(result.getResult().getResultCode(), result.getResult());
                                } else if (result.getResult() != null && result.getResult().getResultCode() == ResultCode.ERROR) {
                                    if (result.getResult() != null) {
                                        apiCallback.onError(result.getResult().getResultCode(),result.getResult());
                                    } else {
                                        apiCallback.onError(ResultCode.ERROR,result.getResult());
                                    }
                                }
                                // Refesh token calling method
                                else if (result.getResult() != null && result.getResult().getResultCode() == ResultCode.NO_AOUTHERISED) {

                                }
                            } else {
                                apiCallback.onError(ResultCode.ERROR,result.getResult());
                            }
                        }
                    });
        } else {
            apiCallback.onError(ResultCode.NO_INTERNET,null);
        }

    }




}