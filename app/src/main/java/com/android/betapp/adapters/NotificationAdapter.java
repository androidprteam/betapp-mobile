package com.android.betapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.betapp.R;

/**
 * Created by star on 7/27/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private Context context;

    public NotificationAdapter(Context context) {
        this.context = context;
    }


    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notofication, parent, false);
        NotificationViewHolder holder = new NotificationViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        if (position == 0) {
            holder.viewDevider.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected View viewDevider;


        public NotificationViewHolder(View itemView) {
            super(itemView);
            viewDevider = itemView.findViewById(R.id.viewDevider);
        }

        @Override
        public void onClick(View view) {

        }
    }

}
