package com.android.betapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.betapp.R;

import org.w3c.dom.Text;

/**
 * Created by star on 8/27/2016.
 */
public class TabFeedAdapter extends RecyclerView.Adapter<TabFeedAdapter.TabFeedViewHolder> {

    private Context context;

    public TabFeedAdapter(Context context) {
        this.context = context;
    }

    @Override
    public TabFeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_tab_feed, parent, false);

        return new TabFeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TabFeedViewHolder holder, int position) {



    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class TabFeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView texTimer;
        public TabFeedViewHolder(View itemView) {
            super(itemView);
            texTimer = (TextView)itemView.findViewById(R.id.tv_TimerFeed);

        }

        @Override
        public void onClick(View view) {

        }
    }
}
