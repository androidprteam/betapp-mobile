package com.android.betapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.betapp.R;

/**
 * Created by star on 7/26/2016.
 */
public class BetsAdapter extends RecyclerView.Adapter<BetsAdapter.BetViewHolder> {

    private Context context;

    public BetsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public BetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_bets_tab, parent, false);
        BetViewHolder holder = new BetViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(BetViewHolder holder, int position) {
        if (position == 0) {
            holder.viewDevider.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    class BetViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected View viewDevider;

        public BetViewHolder(View itemView) {
            super(itemView);
            viewDevider = itemView.findViewById(R.id.viewDevider);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
