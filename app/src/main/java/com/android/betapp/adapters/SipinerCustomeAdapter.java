package com.android.betapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.betapp.R;
import com.android.betapp.model.Country;

import java.util.List;

/**
 * Created by star on 9/30/2016.
 */
public class SipinerCustomeAdapter extends BaseAdapter {
    Context context;
    int flags[];
    String[] countryNames;
    LayoutInflater inflter;
    List<Country> data;

    public SipinerCustomeAdapter(Context applicationContext, List<Country> data) {
        this.context = applicationContext;
        this.data = data;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.adapte_spinner, null);
        TextView names = (TextView) view.findViewById(R.id.company);
//        names.setTextColor(context.getResources().getColor(R.color.black));
        names.setText(data.get(i).getName());
        return view;
    }

}