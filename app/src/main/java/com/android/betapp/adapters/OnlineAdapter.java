package com.android.betapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.betapp.Events.StartBetChatEvent;
import com.android.betapp.R;
import com.android.betapp.bus.BusFactory;

/**
 * Created by star on 8/11/2016.
 */
public class OnlineAdapter extends RecyclerView.Adapter<OnlineAdapter.OnlineViewHolder> {

    private Context context;

    public OnlineAdapter(Context context) {
        this.context = context;
    }

    @Override
    public OnlineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_online, parent, false);
            OnlineViewHolder holder = new OnlineViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(OnlineViewHolder holder, final int position) {

//        if (position==3){
//            holder.ivGroupIC.setVisibility(View.VISIBLE);
//            holder.tvUserName.setText("R1 ROCKZZZZZ");
//        }else {
//            holder.ivGroupIC.setVisibility(View.GONE);
//        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Item "+position, Toast.LENGTH_SHORT).show();
                BusFactory.getBus().post(new StartBetChatEvent("ChatId","betid"));

            }
        });

    }

    @Override
    public int getItemCount() {
        return 15;
    }

    class OnlineViewHolder extends RecyclerView.ViewHolder {

        protected View viewDevider;
        private TextView tvUserName;

        public OnlineViewHolder(View itemView) {
            super(itemView);
            viewDevider = itemView.findViewById(R.id.viewDevider);
            tvUserName= (TextView) itemView.findViewById(R.id.tvUserName);
        }


    }

}
