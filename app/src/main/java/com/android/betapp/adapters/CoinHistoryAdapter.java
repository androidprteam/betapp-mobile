package com.android.betapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.betapp.R;

/**
 * Created by star on 8/29/2016.
 */
public class CoinHistoryAdapter extends RecyclerView.Adapter<CoinHistoryAdapter.CoinsHistorViewHolder> {

    private Context context;

    public CoinHistoryAdapter(Context context) {
        this.context = context;
    }

    @Override
    public CoinsHistorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_coin_history,parent,false);

        return new CoinsHistorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CoinsHistorViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class CoinsHistorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected View viewDevider;


        public CoinsHistorViewHolder(View itemView) {
            super(itemView);
            viewDevider = itemView.findViewById(R.id.viewDevider);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
