package com.android.betapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.betapp.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hp on 07-07-2017.
 */

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.FriendsViewHolder> {

    private Context context;
    private List<HashMap<String, String>> list;

    public FriendsAdapter(Context context, List<HashMap<String, String>> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public FriendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.friends_list, parent, false);
        return new FriendsAdapter.FriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendsViewHolder holder, int position) {
        bindItems(holder,position);

    }
    public void  bindItems(FriendsViewHolder holder, int position){
        String name =list.get(position).get("name");
        holder.tv_name.setText(name.toString());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class FriendsViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name;
        public CircularImageView iv_Cir_ProfileImage;

        public FriendsViewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_FrindsName);
            iv_Cir_ProfileImage = (CircularImageView) itemView.findViewById(R.id.img_friendsImage);
        }
    }
}
